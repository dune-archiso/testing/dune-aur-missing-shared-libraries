#!/usr/bin/bash

docker build -t dunetest . 2>&1 | tee dockerlog.txt
docker run -ti --rm dunetest bash